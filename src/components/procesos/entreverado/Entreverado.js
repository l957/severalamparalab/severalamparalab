import React from 'react';

const Entreverado = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div className="col-md-auto">
                    <h1 className='text-center'>Entreverado. Manifestaciones de la Cocina Popular Bonaverense</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/procesos/entreverado/1.png" className="card-img-top" alt="Entreverado"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">Memorias Audiovisuales. Realización (guion y edición) de cuatro (4) micro documentales inscritos en el proceso de  investigación: Entreverado. Manifestaciones culturales de la cocina popular bonaverense,  entre relatos y vivencias (2020), desarrollado por TUTUABA. Organismo en movimiento,  en el marco de: Beca Pasantía Nacional Ministerio de Cultura, 2019; Convocatoria Somos  cultura, Gobernación de Boyacá, 2020; Beca para publicación de obra de autoras de los  grupos étnicos y población de interés. Ministerio de Cultura 2020-2021.  </p>

                            <div className="text-center">
                            
                    <a id="teatro-griego" href="https://www.sites.google.com/view/tutuaba-organismoenmovimiento/juntanzas/d-e-buenaventura-valle-del-cauca?authuser=0&pli=1"  className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-eye"> Ver video</i> </a>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Entreverado;