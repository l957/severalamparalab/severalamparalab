import React from 'react';

const Vozpolitica = () => {
    return (
        <div id="voz-politica" className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className='text-center'>La Voz Política del Arte y su Cable a Tierra</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/procesos/voz_politica/1.png" className="card-img-top" alt="La Voz Política del Arte y su Cable a Tierra"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">Pero, hablando en serio, ¿para qué sirve la crítica de arte? 
Oscar Wilde 
El salón, tanto como la crítica, contribuyen históricamente a la afirmación de una condición  del arte referida a su relación con las formas y ritmos del territorio y de quienes lo habitan.  Con este horizonte se propone establecer diferentes trayectos a propósito del devenir de los  salones de arte en nuestro país y, específicamente, del XVII Salón Regional de Artistas cuya  exposición Pacífico se constituye en lugar de llegada de las disquisiciones que se plantean en  torno a la condición política del arte y su relación con el territorio cohabitado. 
Realización Ensayo fotográfico, Video Ensayo y Crítica. Catalogo 17 Salón Regional de  Artistas (SRA). Proyecto: La voz política del arte y su cable a tierra. Exposición Pacífico.  XVII Salón Regional de Artistas. Ganador del Premio Nacional de Crítica del Programa de  Estímulos del Ministerio de Cultura, 2021 

 </p>
                        <br></br>
                            <div className="text-center">
                                <a href="https://issuu.com/artesvisualesmincultura/docs/af_catalogo_17_sra_baja_isbn" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Edición Web</i></a>
                                <p></p> 
                                <a id="espejo-roto" href="https://youtu.be/HHBcQFTVM5U-E" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Vozpolitica;