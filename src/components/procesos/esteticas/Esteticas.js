import React from 'react';

const Esteticas = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Estéticas de la violencia en el Valle del Cauca De lo mitológico a lo visceral</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/procesos/esteticas/1.jpg" className="card-img-top" alt="Estéticas de la violencia en el Valle del Cauca De lo mitológico a lo visceral"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">El lugar de lo siniestro en la creación artística, problema que abarca desde la tragedia griega  hasta la contemporaneidad, es presentado en Pura sangre de Luis Ospina (1982) y Carne de  tu carne de Carlos Mayolo (1983), desde la exploración mitológica y visceral del cuerpo, su  relación con el paisaje y con instituciones políticas, socioeconómicas y religiosas, síntomas  de una interpretación de la condición humana. La presente investigación se pregunta,  precisamente, por las estéticas que tienen lugar en la representación de la violencia en la  región vallecaucana convocada en ambas obras: su relación con el cuerpo atravesado por el  pánico, con la mística de la sangre y con el surrealismo de las realidades que devienen en el  territorio en común.
                    Publicado en CANAGUARO Revista de Cine Colombiano N° 4. Diciembre de 2021.  </p>
                            <div className="text-center">
                            <a href="https://canaguaro.cinefagos.net/n04/esteticas-de-la-violencia-en-el-valle-del-cauca-de-lo-mitologico-a-lo-visceral/" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Leer</i></a>
                    <p></p> 
                    <p className="card-text">Participación en calidad de ponente en el Encuentro Catedra Cinemateca, 2021. Bogotá D.C. </p>
                    <a href="https://www.youtube.com/watch?v=3yIZvYinIs4&list=PLq25aViATLIQMtF5QM5Vp SOLev3QWiC4-&index=2" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                    <p></p>
                    <p className="card-text">Participación en calidad de ponente en el Festival Internacional de Cine de Pasto, 2021. Pasto </p> 
                    <a id="entreverado" href="https://www.facebook.com/ficpapasto/videos/627314675306379/"  className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-facebook"> Ver video</i></a>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Esteticas;