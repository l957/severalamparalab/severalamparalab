
import React from 'react';

const CarouselProcesos = () => {
    return (
        <div className="row g-0">
        <div className="col-md-2 "></div>
        <div className="col-md-8 ">                         
                    <div id="carouselExampleDarkProcesos" className="container-fluid carousel carousel-dark slide" data-bs-ride="carousel" >
                        <div className="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="3" aria-label="Slide 4"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="4" aria-label="Slide 5"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="5" aria-label="Slide 6"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="6" aria-label="Slide 7"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="7" aria-label="Slide 8"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="8" aria-label="Slide 9"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="9" aria-label="Slide 10"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="10" aria-label="Slide 11"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="11" aria-label="Slide 12"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="12" aria-label="Slide 13"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="13" aria-label="Slide 14"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="14" aria-label="Slide 15"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide-to="15" aria-label="Slide 16"></button>
                        </div>
                        <div className="carousel-inner">
                            <div className="carousel-item active ratio ratio-16x9"  data-bs-interval="1500">
                            <img src="images/procesos/carousel/1.png" className="d-block w-100"  alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/2.png" className="d-block w-100 " alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/3.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/4.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/5.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/6.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/7.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/8.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/9.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/10.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/11.png" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/12.jpg" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/13.jpg" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/14.jpg" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/15.jpg" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                            <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                            <img src="images/procesos/carousel/16.jpg" className="d-block w-100" alt="procesos investigativos y editoriales" />                            
                            </div>
                        </div>
                        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Anterior</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDarkProcesos" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Siguiente</span>
                        </button>
                    </div>
                    </div>
            <div className="col-md-2  "></div>
        </div> 
                    
          
    );
  }
  
  export default CarouselProcesos;