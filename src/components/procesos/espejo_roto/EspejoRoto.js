import React from 'react';

const EspejoRoto = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Espejo Roto Espejo Enfermo</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/procesos/espejo/1.png" className="card-img-top" alt="Espejo Roto"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">Teniendo como vector de indagación las preguntas: ¿Cómo se retrata el cuerpo?, ¿Cómo se mediatiza el cuerpo?, son objeto de análisis y reflexión dos documentales colombianos:  “Balada para niños muertos” del director caleño Jorge Navas, y “En tránsito”, dirigido por  Liliana Hurtado y Mauricio Vergara. Ambos largometrajes comparten el deseo de construir  un retrato, “hacer volver atrás” el cuerpo y la memoria, “sacar a la luz” lo que esconde la  mirada, y “hacer revivir” la diversidad y contradicción que habitan la existencia. La muerte  y la enfermedad serán motivos recurrentes en ambas obras, y funcionarán como lentes para  retratar el cuerpo y mediatizar la vida.  
Crítica y video ensayo resultado del Encuentro de Crítica e Investigación. Dirección de  audiovisuales, cine y medios interactivos del Ministerio de Cultura. Cali, octubre-noviembre  de 2020
 </p>
                            <div className="text-center">
                            <a href="https://desistfilm.com/espejo-roto-espejo-enfermo-sobre-balada-para-ninos-muertos-y-en-transito/" className="btn btn-primary" target="_blank" without rel="noreferrer">DESISTFILM, Portal web de Cine Latinoamericano: Lima, Perú.</a>
                    <p></p> 
                    <a href="https://mincultura.gov.co/areas/cinematografia/Documents/CUADERNO%20DE%20CRITICA%20ENCUENTROS.pdf?fbclid=IwAR35_EmKWyo-_kF3ZGuWHMWGvQDSXN3oCUEmPLp9yEu_M1mum7U_lu9TUxA" className="btn btn-primary" target="_blank" without rel="noreferrer">CUADERNO DE CRÍTICAS DE CINE, Ministerio de Cultura de Colombia, Encuentros  2020 Dirección de audiovisuales, Cine y Medios Interactivos, CNACC, Temporada de Cine  Crea Colombia, Festival Internacional de Cine de Cali FICCALI. </a>
                    <p></p> 
                    <a href="https://canaguaro.cinefagos.net/blog/balada-para-ninos-muertos-de-jorge-navas-en-transito-de-liliana-hurtado-y-mauricio-vergara/"  className="btn btn-primary" target="_blank" without rel="noreferrer">CANAGUARO. Revista de Cine Colombiano – Portal de Crítica de Cine: Cinefagos.net.  Medellín, Colombia. </a>
                    <p></p> 
                    <a id="esteticas" href="https://girlsatfilms.com/2021/01/04/film-review-encuentros-2020-de-investigacion-y-critica-espejo-roto-espejo-enfermo/"  className="btn btn-primary" target="_blank" without rel="noreferrer">GIRLS AT FILMS. Revista digital y female journal. México D.F.  </a>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default EspejoRoto;