import React from 'react';

const Teatro = () => {
    return (
        <div id="voz-politica" className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">                    
                    <h1 className='text-center'>Del teatro griego al cine experimental</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/procesos/teatro/1.png" className="card-img-top" alt="Del teatro griego al cine experimental"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">El teatro griego constituye un referente en la historia occidental. Empero, sus características:  presencia del coro, carácter sagrado, escenificación de obras reconocidas…, hacen pensar a la  tragedia griega como un fenómeno incapaz de asirse a los ojos del hombre moderno, a no ser  como una barbarie. Esta reflexión la realizaba Nietzsche un siglo antes de internet, y poco más  de veinte años antes de las primeras imágenes en movimiento. En su primera obra Nietzsche  cuestiona la tragedia griega y, desde allí, realiza una crítica al teatro moderno, que posteriormente  derivara en un problema: lo trágico. El ámbito de reflexión que Nietzsche propondrá para este es  la estética, entendiéndola como “fisiología aplicada”. La presencia del cuerpo en la pregunta  estética de Nietzsche, lo llevaran a plantear lo trágico como una “disposición experimental” en  relación con la vida. Una estética experimental que privilegia la desestabilización de las formas  convencionales, implícitas en la realidad y el lenguaje. En este sentido, el cine experimental  pareciese configurarse como una actitud estética que, en función de la construcción de nuevas  corporeidades y realidades, afirma la vida, el movimiento inherente a ella, el azar, la ausencia de orden, estabilidad, identidad: el carácter múltiple de la existencia.  
Participación en calidad de ponente en el IV Encuentro de Filosofía y Cine. Universidad  Autónoma del Estado de México. Toluca, México. 2020; y en el I Congreso de Egresados del  Departamento de Filosofía de la Universidad del Valle  

 </p>
                            <div className="text-center">
                            <a href="https://drive.google.com/file/d/1uuWwXct3bunx9f9ZIpHk6dcj7VvZQoiu/view?usp=sharing" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Leer</i></a>
<p></p> 
<a href="https://m.facebook.com/story.php?story_fbid=380175786562921&id=18897937380065 89" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-facebook"> Ver video</i></a>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Teatro;