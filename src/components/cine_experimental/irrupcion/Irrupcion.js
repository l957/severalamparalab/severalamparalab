import React from 'react';

const Irrupcion = () => {
    return (
        <div id="irrupcion" className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className='text-center'>Irrupción</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-white bg-dark text-center">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/irrupcion/1.jpg" className="card-img-top" alt="Irrupción"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                            <p className="card-text">¿Qué irrumpe? Y aparece lo intempestivo, lo que siempre ha estado. El dolor de estómago,  el encuentro absorto. Impresión. Catapulta. Se desborda, se expande. El latido, el impulso,  movimiento íntimo, calor, ruido. Lo que no sé, lo que no se dice. Estatismo, mutismo, grito,  dónde arderas ahora, es ahora mismo. Caída, explosión, intrusión, espectáculo, pupilas,  dedos. El otro. Quiénes somos, cómo somos. Infiltración, grieta, picar, veneno, alteración,  giro, miedo, frío, punzada, superficie, caos. Crisis del griego Krisis, juicio, decisión,  estupefacción, convulsión, compulsión, trastorno. Despojo. Aborto. Voz, baile, alucinación,  pájaro, afección, tensión disfónica, incertidumbre, separación, herida. Gesto. Parto. Sí  mismo. 
        Proyecto colaborativo de reapropiación Soy Nosotrxs (Argentina, Colombia, México), a  partir de la película ¿Cómo es Gerald? (2017-2021) Meli Marcow. Cine Toro Experimental  Film Festival, ULTRAcinema MX  </p>
                            <br></br>
                            <div className="text-center">
                                <a id="metodo" href="https://youtu.be/MNyX8osIths" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Irrupcion;