import React from 'react';

const Martillos = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Nuestros Martillos Nuestras Brujas</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/martillos/1.jpg" className="card-img-top" alt="Nuestros Martillos Nuestras Brujas"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> A través de una referencia alegórica a la figura de la bruja, retratada en el renacimiento y la  modernidad por medio de la literatura y la plástica, se revelan las mezquindades de un  pensamiento enfermo que niega el cuerpo y condena el poder femenino. Aún arden nuestras  mujeres, el miedo al cuerpo sigue vivo. La bruja somos todas, resistencia viva de la relación  entre la tierra y la humanidad. Quemadas por la historia, nos reproducimos en el tiempo como  la maleza en el monte. Nuestros martillos Nuestro poder. 
Proyecto Transmedia: Ensayo fotográfico, Videoarte, Fanzines e Instalación. Socializado en  el marco del Día de la Mujer en Paipa, Sogamoso y Tunja, Boyacá. 2021 
Guion, Dirección Estética y Edición Videoarte, Fanzines e Instalación: SeveraLÁMPARA. 


 </p>
                        <br></br>
                            <div className="text-center">
                                <a id="absentia" href="https://youtu.be/FRXfLVhP6-E" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Martillos;