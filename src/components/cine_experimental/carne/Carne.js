import React from 'react';

const Carne = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div className="col-md-auto">
                    <h1 className='text-center'>Carne Radioactiva</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/carne/1.jpg" className="card-img-top" alt="Carne Radioactiva"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">Simón del desierto (1965) de Luis Buñuel, se configura en el retrato claustrofóbico del  discurso religioso, capaz de extrapolarse a las figuras del colonialismo y el capitalismo.  Desolado entre el cielo y la tierra, las imágenes que encarnan a Simeón el Estilita sobre una  columna en la mitad del desierto, evidencian el desdén por el cuerpo que se hace minúsculo  ante la inmensidad, despejada y brumosa a la vez, de quien desprecia la vida en procura de  una superioridad desconocida que se alimenta de la austeridad y el castigo a lo corpóreo. El  rechazo de la carne, del instinto, satanizado bajo la figura de la mujer, bosqueja una vitalidad  enferma en cuyas vísceras lo orgánico se torna demonio. Con imponencia se reproduce la  carga histórica e insana que el cristianismo atribuye a lo femenino. Frente a esta lectura del  cuerpo y del deseo de mujer, mi cuerpo se representa desde el baile y el movimiento como contrapeso de la quietud coja, propia de la interpretación proyectada.  
Película colectiva de reapropiación El Desierto de Simón. ULTRAcinema MX, 2021. (Proyectada en México, España, Colombia) 
  </p>
                        <br></br>
                            <div className="text-center">
                                <a id="queer" href="https://youtu.be/l23rLNpnpzI" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Carne;