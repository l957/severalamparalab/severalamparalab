import React from 'react';

const Absentia = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>In Absentia</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/absentia/1.jpg" className="card-img-top" alt="In Absentia"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> "Nosotros los que conocemos somos desconocidos para nosotros mismos […] esto tiene un  buen fundamento. No nos hemos buscado nunca, - ¿Cómo iba a suceder que un día nos  encontrásemos?" 
F. Nietzsche 
"... ¿para qué he de ser siempre yo si sólo puedo ser yo?" 
F. Pessoa 
Me interrogo, me escarbo, me instigo, -¿dónde estás corazón? Mi angustia: gorda, imponente  y mayúscula, dialoga con mi ausencia, la ausencia de mí misma, la que aunque busca no se  encuentra. ¡El vacío está lleno de excesos! Al final, naturaleza salvaje. Retrato múltiple,  orgánico, vivo. La ausencia de lo que se desconoce es el salto al vacío, el ojo que mira hacia  dentro, mi espejo. 
Selección oficial VI Festival de Video arte de Palmira  
Figuraciones. Exposición colectiva jóvenes artistas. Alianza Comfandi - Urbanarte. Palmira. 2021 


 </p>
                        <br></br>
                            <div className="text-center">
                                <a id="salad" href="https://youtu.be/d46UDoHgq_U" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Absentia;