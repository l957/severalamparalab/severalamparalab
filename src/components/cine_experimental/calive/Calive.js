import React from 'react';

const Calive = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Calive</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/calive/1.jpg" className="card-img-top" alt="Calive"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> Mediometraje documental realizado en el marco de procesos de promoción artística y cultural  con habitantes de calle del Hogar de paso Samaritanos de la Calle, ubicado en el Barrio El  Calvario, en Cali, Valle del Cauca. 
Un homenaje a Agarrando Pueblo (1977) de Luis Ospina y Carlos Mayolo, así como a la  literatura de Andrés Caicedo, a partir de un reconocimiento de la ciudad de los años 70, y un  ejercicio de reapropiación y re significación de la geografía cohabitada -política y poética-. 
Red de Bibliotecas Públicas de Cali, Secretaria de Cultura. 2017-2018.

 </p>
                        <br></br>
                            <div className="text-center">
                                <a  href="https://youtu.be/VXW0T3s6NUY" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Calive;