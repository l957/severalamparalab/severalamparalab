import React from 'react';

const Salad = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Salad</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/salad/1.jpg" className="card-img-top" alt="Salad"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> "Tú sabes que ponerse a querer a alguien es una hazaña. Se necesita una energía, una  generosidad, una ceguera. Hasta hay un momento, un principio mismo, en que es preciso  saltar un precipicio; si uno reflexiona, no lo hace."  </p>
                        <p className='card-text text-end'>Jean-Paul Sartre </p>
                        <br></br>
                            <div className="text-center">
                                <a id="calive" href="https://youtu.be/8gfVL7KEdfQ-E" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Salad;