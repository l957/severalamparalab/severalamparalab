import React from 'react';

const Divagacion = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div className="col-md-auto">
                    <h1 className='text-center'>Estados de Divagación</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/divagacion/1.jpg" className="card-img-top" alt="Estados de Divagación"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> Frente a la necesidad de ensayarse, así como de hacer un ejercicio estético y practico de la  filosofía, Estados de divagación se configura en una experiencia personal y visceral en la que  son ausentes el mundo y sus cánones: representación, argumento, identidad acabada y  estática; y a su vez, experiencia colectiva marcada por la presencia de lo múltiple, lo  fragmentario, lo que transita y nos atraviesa. Así mismo la presencia y materialidad de la  obra se soporta a través de sensaciones y conceptos construidos en el vacío y en la sombra.  La ausencia está llena de excesos, y lo presente carente de sí. En cuanto a la composición de  tríada esta responde al objetivo de dar un orden al espejo que se propone entre divagar y  crear.  
Selección oficial V Festival de Video arte de Palmira. 
Selección oficial 8° Festival cortos psicoactivos Échele Cabeza. 
Selección oficial II Festival de cortometrajes de mujeres colombianas directoras y/o  productoras. Categoría Fantasía. 
Selección oficial Festival Transversal Sonora 2022 Modulaciones, franja Sonidos en  Diálogo. 
 </p>
                        <br></br>
                            <div className="text-center">
                                <a id="inclusion" href="https://youtu.be/4wFTqtHKR9o" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Divagacion;