import React from 'react';

const Inclusion = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div className="col-md-auto">
                    <h1 className='text-center'>Inclusión. This is Colombia</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
            <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/inclusion/1.jpg" className="card-img-top" alt="Inclusión. This is Colombia"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text"> El 11 de agosto del año 2020 cinco menores son violentamente asesinados en el barrio Llano  Verde, al oriente de la ciudad de Cali, Colombia. El 11 de agosto del año 2020 usted se  levanta con esa noticia, desayuna: indigestión de cabeza y estómago. El 11 de agosto del año  2020 Colombia es, para no variar, insistentemente amarga, y esa amargura se lo traga a uno,  es incluyente. La continuidad de la política de los "falsos positivos" desangra  permanentemente la juventud colombiana. Bombardeadxs de forma inclemente por el horror  en campos, calles, ventanas y pantallas, quienes nacemos en Colombia habitamos la tierra  del no futuro, ¡el país de la inclusión! Nuestro horizonte se encuentra en tierras anegadizas,  fosas comunes, listas estatales, noticias terroristas.  
La obra es una pregunta por la vida: el cuerpo, el otro, el territorio: geográfico, político,  poético. La pregunta, que surge desde la ventana, justo en frente de la pantalla, se hace  declaración: somos lxs hijxs de la guerra, incluidxs en una incesante cámara ardiente.  Finalmente, con el fuego, la pregunta se hace deseo y manifiesto: por la vida, por el cuerpo,  por el otro, por una nueva forma de inclusión en la que no se excluyan las diferencias. 
Selección oficial VII Festival de Video arte de Palmira. 
Selección oficial V Encuentro de Cine y Filosofía de la Universidad Autónoma del Estado  de México, Toluca. 

 </p>
                        <br></br>
                            <div className="text-center">
                                <a id="martillos" href="https://youtu.be/apMAeVEM_MQ" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Inclusion;