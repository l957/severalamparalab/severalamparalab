import React from 'react';

const Metodo = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>El Método</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3  text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/metodo/1.jpg" className="card-img-top" alt="El Método"/>
                    </div>
                    <div className="col-md-6">
                        <div className="card-body">
                            <p className="card-text">“La memoria se va, las imágenes se quedan…” 
        El archivo tiene vida propia cuando el azar elige el trayecto, entonces la memoria juega con  las imágenes. Fragmentos de la vida que me atraviesa, grabados en video sin ninguna  pretensión definida y por eso destinados a no ser, aun cuando ya son, dialogan con Jonas  Mekas, que a su vez se comunica con J.L. Guerín en 2008. Este entramado de conversaciones  que surgen desde la intimidad son pirotecnia de la memoria, primero sobre el modo de vida  y la forma de mirar de Jonas, sobre su método, y segundo sobre mi vida y mi relación con  las imágenes fabricadas a través de la máquina que Dziga Vertov llamaba “telescopio del  tiempo”. 
        Cortometraje seleccionado para el homenaje a Jonas Mekas, a cien años de su nacimiento,  MEKAS 100. Curaduría Antonio Bunt. Festival ULTRAcinema MX. 2022.  (México/Colombia/España/Suiza/Brasil/EUA/Rumania/Australia) 
        </p>
                            <div className="text-center">
                            <a id="rio" href="https://youtu.be/aHPDVADZerY" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                            
                        </div>

                        
                    </div>
                </div>
                
                
            </div>
            
        </div>             
    );
  }
  
  export default Metodo;