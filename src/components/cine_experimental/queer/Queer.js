import React from 'react';

const Queer = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Futuros Queer. Un Diálogo Para Más de Dos</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/queer/1.jpg" className="card-img-top" alt="Futuros Queer. Un Diálogo Para Más de Dos"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">La resistencia Queer se proyecta en la obra desde el lugar de la pregunta sobre mi misma,  pregunta que encuentra en su desarrollo el rostro de quien ha sido desierto, aislamiento. De  quien sabiéndose diferente quiso dejar de serlo. De quien dejó de querer ser sol para buscar  soledades. El conocimiento de sí mismo no sólo es, por su objeto, un camino lleno de espinas.  Además está la sociedad, sus categorías, prototipos y definiciones que hacen de la vida un  molde, una máscara que cargar. No encajar, afirmar lo desconocido, aun cuando se juzgue  de monstruoso, implica saltar un abismo hacia sí mismo. Implica afirmar el peligro de lo  vivo. Implica retratarse en tránsito. 
Mi retrato tiene memoria, celebra, hay una conquista histórica en él: devenir múltiple,  devenir diálogo, devenir río. 
Obra (Video ensayo) ganadora convocatoria Ciclo Rosa, 2022 (Bogotá, Medellín,  Manizales). Goethe Institut – Kolumbien 
Ciclo Boyacá Cuir, Cine Club Laberinto, Tunja, 2022 

  </p>
                        <br></br>
                            <div className="text-center">
                                <a id="divagacion" href="https://youtu.be/2FBafAFTxCo" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Queer;