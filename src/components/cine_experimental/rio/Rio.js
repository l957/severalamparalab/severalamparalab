import React from 'react';

const Rio = () => {
    return (
        <div className="container">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Andá Buscálo al Río</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="card mb-3 text-center text-white bg-dark">
                <div className="row g-0">
                    <div className="col-md-6">
                        <br></br>
                        <img src="images/experimental/rio/1.jpg" className="card-img-top" alt="Andá Buscálo al Río"/>
                    </div>                    
                    <div className="col-md-6">
                        <div className="card-body">
                        <p className="card-text">En medio del estallido social ocurrido en Colombia durante 2021 fueron encontrados varios  cadáveres de desaparecidos en diferentes ríos de la ciudad de Cali. Frente a este lacerante  acontecimiento la obra de Videoarte propone una extrapolación del lenguaje oral en procesos de construcción poética textual y audiovisual, teniendo como punto de partida la aseveración  “-Andá buscálo al río”, realizada por un agente de las fuerzas armadas en respuesta a la  pregunta de una madre por su hijo desaparecido. Lo anterior a partir de la desterritorialización  de las atmósferas hídricas de la ciudad desde una formulación estética surrealista que alude  a la imagen del río como sepulcro vivo: la brusquedad de la violencia en el agua que se pudre  se hace ruido poético. 
Obra seleccionada para participar en el Contra Salón Nacional de Artistas Guaca-hayo,  Barranquilla, 2022.  </p>
                        <br></br>
                            <div className="text-center">
                                <a id="carne" href="https://youtu.be/COG_Mm_ws8Q" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>             
    );
  }
  
  export default Rio;