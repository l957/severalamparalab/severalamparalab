
import React from 'react';

const Footer = () => {
    return (
        <div id="contactoFooter" className="container">
            <h1  className='text-center'>Contacto</h1>
            <footer className="py-3 my-4">            
                <div className="nav justify-content-center pb-3 mb-3" dangerouslySetInnerHTML={{__html: '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.581904599333!2d-72.94168772400884!3d5.7731334942094525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e6a471cfe6e45fb%3A0xe7132d69a49cf15f!2sSeveraL%C3%A1mpara%3A%20Lab.%20Creativo%20Audiovisual!5e0!3m2!1ses!2sco!4v1692727669783!5m2!1ses!2sco" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>'}}></div>    
                <ul className="nav justify-content-center pb-3 mb-3">
                    <li className="nav-item"><a href="https://goo.gl/maps/pict2zFuV2Z1hrmu8" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-google text-white"></i></a></li>
                    <li className="nav-item"><a href="https://www.facebook.com/profile.php?id=100046370147264" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-facebook text-white"></i></a></li>
                    <li className="nav-item"><a href="https://api.whatsapp.com/send/?phone=573233398593&text&type=phone_number&app_absent=0" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-whatsapp text-white"></i></a></li>
                    <li className="nav-item"><a href="https://www.instagram.com/severalampara.lab/" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-instagram text-white"></i></a></li>
                    <li className="nav-item"><a href="https://www.youtube.com/channel/UC47yOLkIA0lEP2xnLFlxzYw/featured" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-youtube text-white"></i></a></li>
                    <li className="nav-item"><a href="https://open.spotify.com/show/64FD9TuCaPag5sXxNtGtpy" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-spotify text-white"></i></a></li>
                </ul>
                <ul className="nav justify-content-center pb-3 mb-3">
                    <li className="nav-item"><a href="mailto:fan.cine.lab@gmail.com" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-envelope-paper-heart-fill text-white">  fan.cine.lab@gmail.com</i></a></li>                    
                    <br></br>
                    <li className="nav-item"><a href="mailto:severalampara.lab@gmail.com" target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted"><i className="bi bi-envelope-paper-heart-fill text-white" >  severalampara.lab@gmail.com</i></a></li>
                </ul>
                <ul className="nav justify-content-center  pb-3 mb-3">
                    <li className="nav-item"><a target="_blank" without rel="noreferrer" className="nav-link px-2 text-muted " href="tel:3233398593"><i className="bi bi-telephone-inbound-fill text-white ">  +57 323-339-8593</i></a></li>
                </ul>
                <ul className="nav justify-content-center  pb-3 mb-3">
                    <li className="nav-item">
                            <img src="image.png" width="85" height="70" alt="severalámpara" />
                    </li>
                </ul>
                <p className="text-center text-muted">&copy; 2024 Sirada-ARHM</p>
            </footer>
        </div>
    );
  }
  
  export default Footer;