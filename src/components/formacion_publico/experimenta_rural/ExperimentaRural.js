import React from 'react';

const ExperimentaRural = () => {
    return (
        <div className="container-fluid">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Experimenta Rural</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>            
            <div className="card text-center text-white bg-dark">
                <div className="row">
                    <div className="col-lg"></div>
                    <div className="col-lg">
                        <div className="h-50 d-inline-block">
                            <img src="images/formacion/experimenta_rural/1.jpg" className="card-img-top" alt="experimental rural"/>
                        </div>
                    </div>
                    <div className="col-lg"></div>
                </div>
                <div className="row">
                    <div className="col"></div>
                    <div className="col-md-6">                        
                        <div className="card-body">
                            <p className="card-text">En 2022, en el marco de la convocatoria departamental Cultura + Educación Valle Invencible  de la Secretaría de Cultura del Valle del Cauca, se desarrolló el proyecto Experimenta Rural.  Circulación de obras experimentales realizadas por jóvenes rurales, en el Laboratorio de  apreciación y creación de cine y video experimental Fotogramas de la Sultana Rural, 
        realizado el año inmediatamente anterior en la vereda Campo Alegre, corregimiento  Montebello, Cali. A partir de jornadas en Instituciones Educativas rurales y espacios  culturales y comunitarios del departamento del Valle del Cauca, se exhibieron y socializaron  siete (7) obras audiovisuales de carácter experimental. Este proceso tuvo una duración de tres  meses.  </p>
                        </div>
                        <div className='text-center'>
                                <a href="https://youtu.be/3r5EcW6fAT0" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                        </div>
                    </div>
                    <div className="col"></div>
                </div>
            </div>
            
            
        </div>             
    );
  }
  
  export default ExperimentaRural;