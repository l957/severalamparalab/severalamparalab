
import React from 'react';

const CarouselMain = () => {
    return (
        <div className="row g-0">
            <div className="col-md-2 "></div>
            <div className="col-md-8 ">
                <div id="carouselExampleDark" className="container-fluid carousel carousel-dark slide" data-bs-ride="carousel" >
                            <div className="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="5" aria-label="Slide 6"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="6" aria-label="Slide 7"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="7" aria-label="Slide 8"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="8" aria-label="Slide 9"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="9" aria-label="Slide 10"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="10" aria-label="Slide 11"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="11" aria-label="Slide 12"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="12" aria-label="Slide 13"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="13" aria-label="Slide 14"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="14" aria-label="Slide 15"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="15" aria-label="Slide 16"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="16" aria-label="Slide 17"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="17" aria-label="Slide 18"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="18" aria-label="Slide 19"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="19" aria-label="Slide 20"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="20" aria-label="Slide 21"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="21" aria-label="Slide 22"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="22" aria-label="Slide 23"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="23" aria-label="Slide 24"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="24" aria-label="Slide 25"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="25" aria-label="Slide 26"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="26" aria-label="Slide 27"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="27" aria-label="Slide 28"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="28" aria-label="Slide 29"></button>
                                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="29" aria-label="Slide 30"></button>
                            </div>
                            <div className="carousel-inner">
                                <div className="carousel-item active ratio ratio-16x9"  data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/1.jpg" className="d-block w-100"  alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/2.jpg" className="d-block w-100 " alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/3.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/4.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/5.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/6.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/7.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/8.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/9.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/10.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/11.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/12.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/13.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/14.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/15.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/16.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/17.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/18.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/19.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/20.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/21.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/22.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/23.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/24.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/25.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/26.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/27.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/28.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/29.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/formacion/pasarela_min/30.jpg" className="d-block w-100" alt="formación públicos" />                            
                                </div>
                                
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Anterior</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Siguiente</span>
                            </button>
                        </div>
            </div>
            <div className="col-md-2  "></div>
        </div>         
                    
                
                    
          
    );
  }
  
  export default CarouselMain;