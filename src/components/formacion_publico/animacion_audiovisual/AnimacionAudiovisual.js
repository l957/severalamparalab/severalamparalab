import React from 'react';

const AnimacionAudiovisual = () => {
    return (
        <div id="animacion_audiovisual" className="container">
            <br></br>
            <br></br>
            <br></br>
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Laboratorio de Animación Audiovisual</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <div className="card mb-3 text-white bg-dark text-center" >
                <div className="row g-0">
                    <div className="col-md-6 ">
                        <br></br>
                        <img src="images/formacion/animacion_audiovisual/1.jpg" className="card-img-top" alt="laboratorio de animación audiovisual"/>
                    </div>
                    <div className="col-md-6">
                        <div className="card-body">
                            
                            <p className="card-text">Entre marzo y octubre de 2019 se realizó un taller creativo de animación audiovisual que impactó a quince (15) niñas y niños habitantes de la vereda Limones, corregimiento La Castilla, Cali, así como a sus familiares y cuidadores, quienes hicieron parte activa del  proceso formativo. Se desarrollaron actividades de apreciación de obras de animación  audiovisual, construcción gráfica de personajes y utilería, y animación audiovisual cuadro a 
        cuadro (stop motion), a partir del uso de materiales reciclables. Como resultado se presentó  el vídeo Árbol de Limón, relacionado con el cuidado del medio ambiente. </p>
                            <br></br>
                            <div className="text-center">
                                <a  href="https://youtu.be/BjfMsBS5gbw" className="btn btn-primary " target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                    <p id="fotogramas"></p>
                </div>
            </div>
        </div>             
    );
  }
  
  export default AnimacionAudiovisual;