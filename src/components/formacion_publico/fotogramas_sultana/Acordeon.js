import React from 'react';

const Acordeon = () => {
    return (
        <div>
            <div className="row">
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Antes y Ahora</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/antes_ahora/1.jpg" className="card-img-top border" alt="antes y ahora severalámpara"/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">
                                Dir. Nicolle Salcedo 
                            </p>
                            <p className="card-text">
                                A través de una mirada al presente desde su analogía con el pasado, el cortometraje, que se  reapropia de un videoclip antiguo grabado en la vereda Campo alegre, cuestiona la  presencia de la violencia en el territorio, así como la necesidad cultural que lo habita. 
                            </p>
                            <p className="card-text">
                                Cámara: @jhonyperto_fotos @j_camilo_mesa_l 
                            </p>
                            <p className="card-text">
                                Formación y producción: @severalampara.lab
                            </p>
                            <div className='text-center'>
                                <a href="https://youtu.be/vH0bqoC-ZrQ" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Limones Rap</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/limones_rap/1.jpg" className="card-img-top border" alt="..."/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">Dir. Dayana Cárdenas Gutiérrez – Niñas y Niños vereda Limones, Corregimiento La  Castilla, Cali 
    </p>
                            <p className="card-text">
                            “Cruzamos fronteras, no tenemos dueño, rompemos barreras, cumplimos los sueños”. Una  apuesta contracultural del Rap y su contexto de barrio y de calle, narrado desde la ruralidad  y la inocencia de las infancias fugaces. 
                            </p>
                            <p className="card-text">Cámara: @jhonyperto_fotos  
                             </p>
                            <p className="card-text">  
                            Formación y producción: @severalampara.lab  </p>
                            <div className='text-center'><a href="https://youtu.be/TjGKOvcm_oc" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <br></br>
            <div className="row">
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Tres cuadros de la pobreza</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/pobreza/1.jpg" className="card-img-top border" alt="..."/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">
                            Dir. Tatiana Pillimue, Naren Pillimue 
</p>
<p className="card-text">
Una tríada sobre la pobreza material, cultural y emocional, construida a partir de imágenes  cargadas de sensibilidades, contrastes, surrealismo, y experimentación con el cuerpo  propio. 
</p>
<p className="card-text">
Cámara: @j_camilo_mesa_l 
</p>
<p className="card-text">
                            
Formación y producción: @severalampara.lab</p>
                            <div className='text-center'>
                                <a href="https://youtu.be/5PKrHWclCok" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Un balon rodando</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/balon_rodando/1.jpg" className="card-img-top border" alt="..."/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">Dir. Alejandro Pava 
</p>
<p className="card-text">
Una metáfora de la vida, del juego que esta implica, construida desde la mirada que  observa, que es acción y no concepto. 
 </p>
<p className="card-text">
Cámara: @jhonyperto_fotos @j_camilo_mesa_l 
 </p>
<p className="card-text">
Formación y producción: @severalampara.lab </p>
                            <div className='text-center'><a href="https://youtu.be/gapoB_5r30I" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>    
            </div>
            <br></br>
            <div className="row">
            <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Un sentimiento expresado en Colores</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/sentimiento/1.jpg" className="card-img-top border" alt="..."/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">
                            Dir. Brigny Rayo, Juan Camilo Realpe </p>
<p className="card-text">
Frente al dolor que implica el acoso y la agresividad de la sociedad, esta obra surrealista  expresa una salida a través de la figura del espejo, que convoca a una transformación en  nuestra forma de mirar y de mirarnos.  
</p>
<p className="card-text">
                            Cámara: @jhonyperto_fotos @j_camilo_mesa_l  
</p>
<p className="card-text">
                          
Formación y producción: @severalampara.lab</p>
                            <div className='text-center'>
                                <a href="https://youtu.be/WqTF5jwlmXY" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <br></br>
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">Una mirada a Campo Alegre</h5>
                            <br></br>
                            <div className="ratio ratio-4x3">
                                <img src="images/formacion/fotogramas/mirada/1.jpg" className="card-img-top border" alt="..."/>
                            </div>
                            <br></br>
                            <br></br>
                            <p className="card-text">Dir. Danilo Ochoa </p>
<p className="card-text">
La obra ofrece un recorrido por el territorio cohabitado, sus ritmos, dinámicas, imágenes,  caminos y habitantes. Un reconocimiento a la montaña de Campos Alegres.  
</p>
<p className="card-text">Cámara: @jhonyperto_fotos @j_camilo_mesa_l </p>
<p className="card-text">
Formación y producción: @severalampara.lab</p>
                            <div className='text-center'><a href="https://youtu.be/KxTETnGxCZo" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                
            </div>
            <br></br>
            <div className="row">
                <div className="col-3">
                
                </div>
                <div className="col-lg">
                <div className="card bg-dark text-white shadow-lg">
                    <div className="w-75 align-self-center border">
                        <div className="card-body">
                            <h5 className="card-title text-center">ValorArte</h5>
                            <br></br>
                            <img src="images/formacion/fotogramas/valorarte/1.jpg" className="card-img-top border" alt="..."/>
                            <br></br>
                            <br></br>
                            <p className="card-text">Dir. Ana María Certuche   </p>
    <p className="card-text">
    Una mirada sobre la ciudad, sus calles y sus artistas. Una invitación al conocimiento y  reconocimiento del arte que pulula entre el pavimento y la cotidianidad, y que se sobrepone a la violencia. Un canto de Milicianos del Arte. 
   </p>
    <p className="card-text">Cámara: @jhonyperto_fotos       </p>
    <p className="card-text">
    Formación y producción: @severalampara.lab     </p>
                            <div id="experimenta_rural" className='text-center'><a href="https://youtu.be/SFjTwzdKz1c" target="_blank" className="btn btn-primary" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div className="col-3">
                
                </div>
            </div>
           
        </div> 
                
    );
  }
  
  export default Acordeon;