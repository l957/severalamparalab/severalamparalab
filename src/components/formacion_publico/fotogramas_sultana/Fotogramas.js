import React from 'react';

const Fotogramas = () => {
    return (
        <div className="container-fluid">
            <div className="row justify-content-md-center">
                <div className="col col-lg-2">
                    
                </div>
                <div  className="col-md-auto">
                    <h1 className='text-center'>Fotogramas de la Sultana Rural</h1>
                </div>
                <div className="col col-lg-2">
                    
                </div>
            </div>
            <div className="card text-center text-white bg-dark">
                <div className="row">
                    <div className="col-lg"></div>
                    <div className="col-lg">
                        <div className="h-50 d-inline-block ">
                            <img src="images/formacion/fotogramas/1.jpg" className="card-img-top" alt="fotogramas sultana rural"/>
                        </div>
                    </div>
                    <div className="col-lg"></div>
                </div>
                <div className="row">
                    <div className="col"></div>
                    <div className="col-md-6">
                        <div className="card-body">
                            <p className="card-text">En 2021, en el marco de la convocatoria departamental Cultura Valle Invencible de la  Secretaría de Cultura del Valle del Cauca, se desarrolló el Laboratorio de apreciación y  creación de cine y video experimental Fotogramas de la Sultana Rural con once (11)  adolescentes, jóvenes y adultos, habitantes de la vereda Campo alegre, corregimiento  Montebello, a partir del cual se desarrollaron siete (7) obras audiovisuales de carácter  experimental que fueron socializadas con la comunidad, beneficiando a más de 70  participantes de la población asentada en el territorio señalado. Este proceso tuvo una  duración de tres meses. </p>
                        </div>
                    </div>
                    <div className="col"></div>
                </div>
                
            </div>                      
        </div>  
            
    );
  }
  
  export default Fotogramas;