import React from 'react';

const Videoensayos = () => {
    return (
        <div className="container">
            <div className="text-center">
                <h1>VIDEOENSAYOS</h1>
                <br></br>
            </div>
            <div className="row">
                <div className="col">
                
                </div>
                <div className="col-lg">
                <div className="card bg-dark text-white shadow-lg ">
                    <div className="card-body">
                        <h5 className="card-text text-center">Video ensayo etnográfico  </h5>                        
                        <br></br>
                        <img src="images/fancine/videoensayos/1.jpg" className="card-img-top " alt="FAN CINE Vol. 1"/>
                        <br></br>
                        <br></br>
                        <h5 className="card-title text-center">FAN CINE Vol. 1, 2021</h5>
                        <div className='text-center'><a href="https://youtu.be/kz5e_NB7PaA" target="_blank" without rel="noreferrer" className="btn btn-primary"><i className="bi bi-youtube"> Ver video</i></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="col">
                
                </div>
            </div>
            
            
            <br></br>
            <div className="row">
                <div className="col-sm-1">

                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <div className="card-body">
                        <h5 className="card-title text-center">Entre Féminas</h5>
                        <br></br>
                        <img src="images/fancine/videoensayos/2.jpg" className="card-img-top " alt="..."/>
                        <br></br>
                        <br></br>
                        <h5 className="card-title text-center">FAN CINE Vol. 2, 2022</h5>
                        <div className='text-center'><a className="btn btn-primary" href="https://youtu.be/V2LmQaDQsoQ" target="_blank" without rel="noreferrer"><i className="bi bi-youtube"> Ver video</i></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="col-sm-3">
                    <br></br>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                    <div className="card-body">
                        <h5 className="card-title text-center">Epístolas de la primera lectora</h5>
                        <br></br>
                        <img src="images/fancine/videoensayos/3.jpg" className="card-img-top " alt="..."/>
                        <br></br>
                        <br></br>
                        <h5 className="card-title text-center">FAN CINE Vol. 2, 2022</h5>
                        <div className='text-center'><a className="btn btn-primary" href="https://youtu.be/-crUTBYMFE0" target="_blank" without rel="noreferrer" ><i className="bi bi-youtube"> Ver video</i></a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <br></br>
            <br></br>
            <div className="row">
                <div className="col">
                
                </div>
                <div className="col-lg">
                <div className="card bg-dark text-white shadow-lg ">
                    <div className="card-body">
                        <h5 className="card-text text-center">Caribe Experimental</h5>                        
                        <br></br>
                        <img src="images/fancine/videoensayos/4.png" className="card-img-top " alt="FAN CINE Vol. 3"/>
                        <br></br>
                        <br></br>
                        <h5 className="card-title text-center">FAN CINE Vol. 3, 2023</h5>
                        <div className='text-center'><a href="https://youtu.be/VJ-YgBjVrfc" target="_blank" without rel="noreferrer" className="btn btn-primary"><i className="bi bi-youtube"> Ver video</i></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="col">
                
                </div>
            </div>
        </div> 
                
    );
  }
  
  export default Videoensayos;