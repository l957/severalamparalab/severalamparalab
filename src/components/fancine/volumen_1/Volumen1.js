import React from 'react';

const Volumen1 = () => {
    return (
        <div  className="container">
            <br></br>
            <div className="row">
            <div className="col"></div>
            <div className="col-6">
                <p id="volumen1" className="card-text text-center">FAN_CINE es un proyecto de investigación y crítica de cine y video experimental colombiano, cuyo propósito es reconocer y visibilizar este movimiento artístico y sus diálogos con el devenir del territorio.  </p>
            </div>
            <div className="col"></div>
            </div>
            
            <br></br>
            
            <br></br>
            <div className="row">
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                        <br></br>
                        <div className="w-75 align-self-center ">
                            <div className="card-body">                            
                                <div className="">
                                    <img src="images/fancine/volumen1/1.jpg" className="card-img-top border" alt="Vol. 1, 2021"/>
                                </div>
                                <br></br>
                                <h5 className="card-title text-center">FAN_CINE </h5>
                                <h5 className="card-title text-center">Vol. 1, 2021 </h5>
                                <h5 className="card-title text-center">Caliwood: Surrealismo Documental. Años 70 y 90 </h5>

                                <br></br>
                                
                                <div className='text-center'>
                                <a href="https://www.flipsnack.com/68655ACC5A8/fan_cine-cr-tica-de-cine-y-video-experimental-colombiano-vol-1.html" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Edición PDF</i></a>
                                <p></p> 
                                <a id="volumen2" href="https://drive.google.com/file/d/19UdYdQmVBaNPWpbJwRkX5W58NR3LI2Of/view?usp=sharing" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-cloud-arrow-down-fill"> Descargar</i></a>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                        <br></br>
                        <div className="w-75 align-self-center">
                        <div className="card-body">                            
                                <div className="">
                                    <img src="images/fancine/volumen2/1.jpg" className="card-img-top " alt="Vol. 2, 2022"/>
                                </div>
                                <br></br>
                                <h5 className="card-title text-center">FAN_CINE </h5>
                                <h5 className="card-title text-center">Vol. 2, 2022 </h5>
                                <h5  className="card-title text-center">Sultana Caníbal, Valle Experimental</h5>

                                <br></br>
                                <div className='text-center'>
                                <a href="https://www.flipsnack.com/68655ACC5A8/fan_cine-cr-tica-de-cine-y-video-experimental-colombiano.html" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Edición PDF</i></a>
                                <p></p> 
                                <a href="https://drive.google.com/file/d/1OuBdAZbARbV_jCFAlP1Y1mZUvmUjammb/view?usp=sharing" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-cloud-arrow-down-fill"> Descargar</i></a>                            
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg">
                    <div className="card bg-dark text-white shadow-lg ">
                        <br></br>
                        <div className="w-75 align-self-center">
                        <div className="card-body">                            
                                <div className="">
                                    <img src="images/fancine/volumen3/1.png" className="card-img-top " alt="Vol. 3, 2023"/>
                                </div>
                                <br></br>
                                <h5 className="card-title text-center">FAN_CINE </h5>
                                <h5 className="card-title text-center">Vol. 3, 2023 </h5>
                                <h5  className="card-title text-center">Caribe. Poéticas Experimentales</h5>

                                <br></br>
                                <div className='text-center'>
                                <a href="https://online.fliphtml5.com/qhafd/ndlr/" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-book"> Edición PDF</i></a>
                                <p></p> 
                                <a id="videoensayos" href="https://drive.google.com/file/d/12UB9niAqhc8a_hDnfCuv7cA31nnsHd7T/view?usp=sharing" className="btn btn-primary" target="_blank" without rel="noreferrer"><i className="bi bi-cloud-arrow-down-fill"> Descargar</i></a>                            
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
           
            
        </div>             
    );
  }
  
  export default Volumen1;