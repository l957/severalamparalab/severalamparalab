
import React from 'react';

const CarouselFancine = () => {
    return (
        <div className="row g-0">
            
            <div className="col-md-2 "></div>
            <div className="col-md-8 "> 
                <div className="row">
                    <div className="col-sm-3">
                        
                    </div>
                    <div className="col-lg">
                    <div id="carouselExampleDarkfancine" className=" container-fluid carousel carousel-dark slide" data-bs-ride="carousel" >
                        <div className="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="3" aria-label="Slide 4"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="4" aria-label="Slide 5"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="5" aria-label="Slide 6"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="6" aria-label="Slide 7"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="7" aria-label="Slide 8"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="8" aria-label="Slide 9"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="9" aria-label="Slide 10"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="10" aria-label="Slide 11"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="11" aria-label="Slide 12"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="12" aria-label="Slide 13"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="13" aria-label="Slide 14"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="14" aria-label="Slide 15"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="15" aria-label="Slide 16"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="16" aria-label="Slide 17"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="17" aria-label="Slide 18"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="18" aria-label="Slide 19"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="19" aria-label="Slide 20"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="20" aria-label="Slide 21"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="21" aria-label="Slide 22"></button>
                            <button type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide-to="22" aria-label="Slide 23"></button>
                        </div>
                        <div className="carousel-inner">
                            <div className="carousel-item active "  data-bs-interval="1500">
                            <img src="images/fancine/carousel/1.jpg" className="d-block w-100"  alt="fancine severalámpara" />                            
                            </div>
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/2.jpg" className="d-block w-100 " alt="fancine severalámpara" />                            
                            </div>
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/3.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div>
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/4.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div>
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/5.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/6.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/7.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/8.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/9.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/10.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/11.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/12.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                            <img src="images/fancine/carousel/13.jpg" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div>                             
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/14.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/15.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/16.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/17.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/18.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/19.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/20.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/21.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/22.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                            <div className="carousel-item " data-bs-interval="1500">
                                <img src="images/fancine/carousel/23.png" className="d-block w-100" alt="fancine severalámpara" />                            
                            </div> 
                        </div>
                        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Anterior</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDarkfancine" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Siguiente</span>
                        </button>
                    </div>    
                    </div>
                    <div className="col-sm-3">
                        <br></br>
                    </div>
                </div>                 
                    
                    </div>
            <div className="col-md-2  "></div>
        </div>     
                    
          
    );
  }
  
  export default CarouselFancine;