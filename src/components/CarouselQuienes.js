
import React from 'react';

const CarouselQuienes = () => {
    return (
        <div className="row g-0">
            <div className="col-md-2 "></div>
            <div className="col-md-8 ">
            <div id="carouselQuienesDark" className="container-fluid carousel carousel-dark slide" data-bs-ride="carousel" >
                            <div className="carousel-indicators">
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="5" aria-label="Slide 6"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="6" aria-label="Slide 7"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="7" aria-label="Slide 8"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="8" aria-label="Slide 9"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="9" aria-label="Slide 10"></button>
                                <button type="button" data-bs-target="#carouselQuienesDark" data-bs-slide-to="10" aria-label="Slide 11"></button>

                                                            </div>
                            <div className="carousel-inner">
                                <div className="carousel-item active ratio ratio-16x9"  data-bs-interval="1500">
                                <img src="images/quienes/pasarela/1.jpg" className="d-block w-100"  alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/2.jpg" className="d-block w-100 " alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/3.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/4.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/5.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/6.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/7.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/8.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/9.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/10.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                <div className="carousel-item ratio ratio-16x9" data-bs-interval="1500">
                                <img src="images/quienes/pasarela/11.jpg" className="d-block w-100" alt="severalámpara" />                            
                                </div>
                                
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselQuienesDark" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Anterior</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselQuienesDark" data-bs-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Siguiente</span>
                            </button>
                        </div>
            </div>
            <div className="col-md-2  "></div>
        </div>         
                    
                
                    
          
    );
  }
  
  export default CarouselQuienes;