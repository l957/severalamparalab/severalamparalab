
import React from 'react';

const QuienesSomos = () => {
    return (
        <div classNameName="container">
            <div class="row">
                <div class="col-md">
               
                </div>
                <div class="col-md-6">
                    <h1 id="QuienesSomos" className='text-center'>Somos ensayo y laboratorio.</h1>
                    <h1 className='text-center'>Estamos destinadas a la duda y a la experimentación.</h1>
                    <h1 className='text-center'>***</h1>                
                    <p className="text-center">SEVERALÁMPARA laboratorio creativo nace en el año 2018 en la Vereda Campo Alegre, en la ciudad de Cali, Colombia, como un espacio de indagación, experimentación e investigación en torno a la creación audiovisual. A lo largo de estos años ha implementado tres líneas de acción: Formación de Públicos, Creación de Obras de Videoarte y Cine Experimental, e Investigación y Desarrollo de Procesos Editoriales. Lo anterior ha posibilitado el desarrollo de laboratorios creativos con poblaciones rurales, el reconocimiento regional, nacional e internacional de algunas de las obras realizadas, la participación en procesos comunitarios y académicos, y la construcción de publicaciones de carácter independiente.</p> 
                    <div class="container">                    
                    <div class="row">
                        <div class="col-sm">
                            <div className="card bg-dark text-white shadow-lg ">
                                <div className="card-body">
                                    <h5 className="card-title text-center">SEVERALÁMPARA</h5>
                                    <br></br>
                                    <img src="images/quienes/equipo/1.png" className="card-img-top " alt="SEVERALÁMPARA"/>
                                    <br></br>
                                    <br></br>
                                    <p className="card-text text-center">  Multiplicidad amateur, pluralidad y tránsito de la que ama y explora. </p>
                                    <div className='text-center'><a href='https://www.instagram.com/severalampara.lab/' target="_blank" without rel="noreferrer" className="btn btn-primary"><i className="bi bi-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div className="card bg-dark text-white shadow-lg ">
                                <div className="card-body">
                                    <h5 className="card-title text-center">ESTRELLA POLAR</h5>
                                    <br></br>
                                    <img src="images/quienes/equipo/2.png" className="card-img-top " alt="ESTRELLA POLAR"/>
                                    <br></br>
                                    <br></br>
                                    <p className="card-text text-center"> Camarada de los sueños y la locura, camarógrafa, editora, performer, diseñadora de producción, diagramadora, luz y sostén. </p>
                                    <div className='text-center'><a href='https://www.instagram.com/estrella.polar94/' target="_blank" without rel="noreferrer" className="btn btn-primary"><i className="bi bi-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                        <div className="card bg-dark text-white shadow-lg ">
                                <div className="card-body">
                                    <h5 className="card-title text-center">TUTUABA</h5>
                                    <br></br>
                                    <img src="images/quienes/equipo/3.png" className="card-img-top " alt="TUTUABA"/>
                                    <br></br>
                                    <br></br>
                                    <p className="card-text text-center">  Alimento, palabra, compromiso y lealtad. </p>
                                    <div className='text-center'><a href='https://www.instagram.com/tutuaba_organismo/' target="_blank" without rel="noreferrer" className="btn btn-primary"><i className="bi bi-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <h2 className="text-center">LÁMPARAS ALIADAS</h2>
                    <br></br>
                    <p className="text-center">Laura Indira Guauque Socha</p>
                    <p className="text-center">Alejandra del Pilar Guauque Socha</p>
                    <p className="text-center">Susana Carolina Guauque Socha</p>
                    <p className="text-center">Evangelina Socha Mariño </p>
                    <p className="text-center">Jorge Eliecer Guauque Agudelo </p>
                    <p className="text-center">Stefany Velásquez Vallejo </p>
                    <p className="text-center">Isabel Patiño Collazos </p>
                    <p className="text-center">Fundación Germán Patiño </p>
                    <p className="text-center">Colectivo Al Otro Lado </p>
                    <p className="text-center">Camilo Tovar </p>
                    <p className="text-center">Urbanarte Casa Cultural </p>
                    <p className="text-center">Judith Guayara Losada  </p>
                    <p className="text-center">María Alejandra Gutiérrez Correa </p>
                    <p className="text-center">Estefania Tibavija Hernández </p>
                    <p className="text-center">Mariarte Producciones </p>
                    <p className="text-center">Nicolas Spinel </p>
                    <p className="text-center">RABIA ACCA </p>
                    <p className="text-center">Biblioteca Comunitaria Campo Alegre </p>
                    <p className="text-center">Colectivo Elementos Te Une</p>
                    <p className="text-center">Andrés Úzuga </p>
                    <p className="text-center">Cine Toro Experimental Film Festival</p>
                    <p className="text-center">Maar Montoya </p>
                    <p className="text-center">Vanessa Zea </p>
                    <p className="text-center">Sirada-ARHM </p>
                    <p className="text-center">Festival UltraCINEMA MX </p>
                    <p className="text-center">Jhon Alexander Angarita Correa</p>                    
                    <br>
                    </br>
                    <div className="row g-0">
            
            <div className="col-md ">
                
            </div>
        </div>
                    </div>
                </div>
                <div class="col-md">
                
                </div>
            </div>
            
        </div>
    );
  }
  
  export default QuienesSomos;
