
import React from 'react';

const Contacto = () => {
    return (
        <div classNameName="container">
            <div  id="contacto" className='row'><br></br><br></br><br></br></div>
            <div  className="row">
                <div className="col">
               
                </div>
                <div className="col-6">
                <h1  className='text-center'>Contacto</h1>
                <br></br>
                <p className="card-text">En el siguiente formulario puedes dejarnos tus comentarios: </p>
                    
                <form>
                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Correo electrónico</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required/>
                        <div id="emailHelp" className="form-text">Ingresa tu correo electrónico</div>
                    </div>
                    <div className="mb-3">
                        <label for="exampleInputname1" className="form-label">Nombre</label>
                        <input type="name" className="form-control" id="exampleInputname1" required/>
                    </div>

                    <div className="mb-3">
                        <span className="input-group-text bg-dark text-white">Mensaje</span>
                        <textarea className="form-control" aria-label="With textarea" required></textarea>
                        <div id="emailMessage" className="form-text">Dejanos un mensaje para conocer más de ti</div>
                    </div>

                    
                    
                    <div className="text-center">
                        <button type="submit" className="btn btn-primary ">Enviar</button>
                    </div>
                </form>  
                </div>
                <div className="col">
                
                </div>
            </div>
            
        </div>
    );
  }
  
  export default Contacto;